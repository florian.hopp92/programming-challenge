package de.exxcellent.challenge;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.exxcellent.challenge.analyzer.FootballAnalyzer;
import de.exxcellent.challenge.analyzer.WeatherAnalyzer;
import de.exxcellent.challenge.reader.CsvReader;

/**
 * Example JUnit 5 test case.
 * 
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
class AppTest {
	private String resourcePath = "./src/main/resources/de/exxcellent/challenge/";

	@BeforeEach
	void setUp() {
		App.result = "";
	}

	@Test
	void runInputReader() {
		List<String[]> rightData = new ArrayList<String[]>();
		rightData.add(new String[] { "1", "88", "59" });
		rightData.add(new String[] { "2", "79", "63" });
		rightData.add(new String[] { "3", "79", "83" });
		rightData.add(new String[] { "4", "79", "-12" });
		rightData.add(new String[] { "5", "-1", "-11" });
		CsvReader csv = new CsvReader("./src/main/resources/de/exxcellent/challenge/weatherTestData.csv");
		List<String[]> testData = csv.read();
		assertEquals(5, testData.size(), "Too few Testdata.");
		for (int i = 0; i < rightData.size(); i++) {
			for (int j = 0; j < rightData.get(i).length; j++) {
				assertEquals(rightData.get(i)[j], testData.get(i)[j],
						"Entry at row " + i + " and column " + j + " is wrong.");
			}
		}
	}

	@Test
	void runWeatherAnalyzer() {
		CsvReader csv = new CsvReader(resourcePath + "weather.csv");
		List<String[]> data = csv.read();
		WeatherAnalyzer weatherAnalyzer = new WeatherAnalyzer();
		assertEquals("14", weatherAnalyzer.analyze(data), "Wrong result of the analyzer.");
	}

	@Test
	void runFootballAnalyzer() {
		CsvReader csv = new CsvReader(resourcePath + "football.csv");
		List<String[]> data = csv.read();
		FootballAnalyzer footballAnalyzer = new FootballAnalyzer();
		assertEquals("Aston_Villa", footballAnalyzer.analyze(data), "Wrong result of the analyzer.");
	}

	@Test
	void runWeatherAnalyzerWithTestData() {
		CsvReader csv = new CsvReader(resourcePath + "weatherTestData.csv");
		List<String[]> data = csv.read();
		WeatherAnalyzer analyzer = new WeatherAnalyzer();
		assertEquals("5", analyzer.analyze(data), "Wrong result of the analyzer.");
	}

	@Test
	void runFootballAnalyzerWithTestData() {
		CsvReader csv = new CsvReader(resourcePath + "footballTestData.csv");
		List<String[]> data = csv.read();
		FootballAnalyzer analyzer = new FootballAnalyzer();
		assertEquals("Newcastle", analyzer.analyze(data), "Wrong result of the analyzer.");
	}

	@Test
	void runWeather() {
		App.main("--weather", "weather.csv");
		assertEquals("14", App.result, "Main method saved the wrong result.");
	}

	@Test
	void runFootball() {
		App.main("--football", "football.csv");
		assertEquals("Aston_Villa", App.result, "Main method saved the wrong result.");
	}

}