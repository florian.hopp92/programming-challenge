package de.exxcellent.challenge.analyzer;

import java.util.List;

public class FootballAnalyzer extends Analyzer {
	public String analyze(List<String[]> data) {
		double minDifference = Double.MAX_VALUE;
		String team = "";
		for (String[] line : data) {
			try {
				double goals = Double.parseDouble(line[5]);
				double goalsAgainst = Double.parseDouble(line[6]);
				double difference = Math.abs(goals - goalsAgainst);
				if (goals < 0 || goalsAgainst < 0) {
					throw new Exception("Wrong input data! Goals and/or goals against are negative.");
				}
				if (difference > minDifference) {
					continue;
				}
				minDifference = difference;
				team = line[0];
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return team;
	}
}
