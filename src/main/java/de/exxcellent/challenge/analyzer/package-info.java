/**
 * The package for all classes related to analyze data inputs.
 *
 * @author Florian Hopp <florian.hopp92@web.de>
 */
package de.exxcellent.challenge.analyzer;