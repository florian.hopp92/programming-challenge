package de.exxcellent.challenge.analyzer;

import java.util.List;

abstract public class Analyzer {
	abstract public String analyze(List<String[]> data);
}
