package de.exxcellent.challenge.analyzer;

import java.util.List;

public class WeatherAnalyzer extends Analyzer {
	public String analyze(List<String[]> data) {
		double minDifference = Double.MAX_VALUE;
		String dayNumber = "";
		for (String[] line : data) {
			try {
				double difference = Double.parseDouble(line[1]) - Double.parseDouble(line[2]);
				if (difference < 0) {
					throw new Exception("Wrong input data! MaxTemp is smaller than minTemp.");
				}
				if (difference > minDifference) {
					continue;
				}
				minDifference = difference;
				dayNumber = line[0];
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dayNumber;
	}

}
