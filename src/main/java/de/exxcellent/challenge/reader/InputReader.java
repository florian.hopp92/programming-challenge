package de.exxcellent.challenge.reader;

import java.util.List;

abstract public class InputReader {
	private String filePath;

	public InputReader(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePath() {
		return this.filePath;
	}

	abstract List<String[]> read();
}
