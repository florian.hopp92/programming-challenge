/**
 * The package for all classes related for processing input files and extracting
 * data from input files.
 *
 * @author Florian Hopp <florian.hopp92@web.de>
 */
package de.exxcellent.challenge.reader;