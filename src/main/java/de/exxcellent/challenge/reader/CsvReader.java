package de.exxcellent.challenge.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CsvReader extends InputReader {
	public CsvReader(String filePath) {
		super(filePath);
	}

	public List<String[]> read() {
		List<String[]> data = new ArrayList<String[]>();
		try {
			BufferedReader csvReader = new BufferedReader(new FileReader(this.getFilePath()));
			String line = "";
			boolean start = true;
			while ((line = csvReader.readLine()) != null) {
				if (start) {
					start = false;
					continue;
				}
				data.add(line.split(","));
			}
			csvReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
}
