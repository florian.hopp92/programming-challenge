package de.exxcellent.challenge;

import java.util.List;

import de.exxcellent.challenge.analyzer.FootballAnalyzer;
import de.exxcellent.challenge.analyzer.WeatherAnalyzer;
import de.exxcellent.challenge.reader.CsvReader;

/**
 * The entry class for your solution. This class is only aimed as starting point
 * and not intended as baseline for your software design. Read: create your own
 * classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {
	public static String result = "";
	private static final String resourcePath = "./src/main/resources/de/exxcellent/challenge/";

	/**
	 * This is the main entry method of your program.
	 * 
	 * @param args The CLI arguments passed
	 */
	public static void main(String... args) {

		// Your preparation code …
		if (args.length < 2) {
			System.out.println("No Arguments found! Proceed with weather arguments.");
			args = new String[2];
			args[0] = "--weather";
			args[1] = "weather.csv";
		}

		CsvReader csv = new CsvReader(resourcePath + args[1]);
		List<String[]> data = csv.read();

		if (args[0].compareTo("--weather") == 0) {
			WeatherAnalyzer wAnalyzer = new WeatherAnalyzer();

			String dayWithSmallestTempSpread = wAnalyzer.analyze(data); // Your day analysis function call …
			System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);
			result = dayWithSmallestTempSpread;
		}
		if (args[0].compareTo("--football") == 0) {
			FootballAnalyzer fAnalyzer = new FootballAnalyzer();

			String teamWithSmallestGoalSpread = fAnalyzer.analyze(data); // Your goal analysis function call …
			System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallestGoalSpread);
			result = teamWithSmallestGoalSpread;
		}

	}
}
